# MVC Code Generator 

MVC Code Generator ermöglicht eine Erstellung einer Projektstruktur im MVC-Pattern. Zu Beginn wird der Namen der Anwendung eingegeben. Daraufhin können einzelne Module erstellt werden, indem man den Namen des Moduls eingibt und nach Bedarf Klassenparameter und Klassenvariablen hinzufügt. Weiter werden nach Funktionen gefragt, die dem Modul hinzugefügt werden können, auch hier wahlweise mit Parametern und Variablen. Nach Erstellung der benötigten Modul wird durch Klick auf den Button „Projekt erstellen“ ein ZIP-Datei zum Download mit der fertigen Struktur bereitgestellt.

Die Anwendung kann einfach über den Browser aufgerufen werden und bedarf keiner weiteren Installationen. 

Über [http://132.199.139.24:8765](Link URL) ist MVC Code Generator aufrufbar.