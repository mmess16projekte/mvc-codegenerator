var App = App || {};
App.Model = function(){
    var that = {},
        modules = [],
        methods = [],
        parAndVar = [],
        para = [],
        vara = [],
        classPara = [],
        classVar = [],
        allMethods = [],
        currentArrayPosition=-1;
    
    function init(){
        
        return that;
    }
    
    function getArray(){
        return modules;
    }
    
   
    function addModuleToArray(moduleName){
        currentArrayPosition+= 4;
        classPara = [];
        classVar = [];
        allMethods = [];
        modules.push(moduleName);
        modules.push(classPara);
        modules.push(classVar);
        

     
    }

    function addFunctionToArray(functionName){
        var method;
        methods = [];
        parAndVar = [];
        para = [];
        vara = [];
        method = {"name" : functionName,
                      "public" : true};
        methods.push(method);
        methods.push(parAndVar);
        allMethods.push(methods);
        parAndVar.push(para);
        parAndVar.push(vara);
        if(modules[currentArrayPosition] ==null){
            modules.push(allMethods);
        } else{modules[currentArrayPosition] = allMethods;
        }
        
    }
    
    
    function addParameterToArray(parameterName){
        
        para.push(parameterName);
        
    }
    
    function addClassVariableToArray(classVariables, isReturned){
        var variable ={"name" : classVariables,
                        "getsReturned" : isReturned};
        classVar.push(variable);
        
    }
    
    function addClassParameterToArray(classParamsName){
        classPara.push(classParamsName);
      
    }
    
    function addVariablesToArray(variable){
        var currentVariable =variable;
        
        vara.push(currentVariable);
       
    }
    
    
    
    that.init = init;
    that.addModuleToArray = addModuleToArray;
    that.addFunctionToArray = addFunctionToArray;
    that.addParameterToArray = addParameterToArray;
    that.addVariablesToArray = addVariablesToArray;
    that.addClassVariableToArray = addClassVariableToArray;
    that.addClassParameterToArray = addClassParameterToArray;
    that.getArray = getArray;
    return that;
    
};
