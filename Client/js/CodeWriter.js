var App = App || {};
App.CodeWriter = function(){

    "use strict";

    
    var that = {},
        noVarReturnedYet;




 
    function parseJsContent(options){
        var content,
            moduleCount = 0,
            contentArray = options,
            className = contentArray[0],
            classParameterCount,
            classVariablesCount,
            methodCount,
            parameterCount,
            variablesCount,
            returnedVariablesCount,
            returnedClassVariablesCount;

        content = "var App = App||{}; \\nApp."+className+" = function(";
        if(contentArray[(4*moduleCount + 1)][0] != null){
            content += contentArray[(4*moduleCount + 1)][0];
            for(classParameterCount = 1; classParameterCount <contentArray[(4*moduleCount + 1)].length; classParameterCount++){
                content +=","+ contentArray[(4*moduleCount + 1)][classParameterCount];
            }
        }
        content +="){\\n\\n      var that ={}";
        if(contentArray[(4*moduleCount + 2)] != null){	
            for(classVariablesCount = 0; classVariablesCount <contentArray[(4*moduleCount + 2)].length; classVariablesCount++){
                content +=",\\n    " +contentArray[(4*moduleCount + 2)][classVariablesCount].name;
            }
        }
        content += ";\\n\\n";

        if(contentArray[moduleCount*4+3] != null){
            for(methodCount = 0; methodCount < contentArray[moduleCount*4+3].length; methodCount++){
                noVarReturnedYet = true;

                content += "function " + contentArray[moduleCount*4+3][methodCount][0].name +"(";

                if(contentArray[moduleCount*4+3][methodCount][1][0] != null){
                    content += contentArray[moduleCount*4+3][methodCount][1][0][0];
                    for(parameterCount = 1; parameterCount < contentArray[moduleCount*4+3][methodCount][1][0].length; parameterCount++){
                        content += ","+contentArray[moduleCount*4+3][methodCount][1][0][parameterCount];
                    }
                } 
                content += "){\\n\\n       ";
                if(contentArray[moduleCount*4+3][methodCount][1][1] != null){
                    if(contentArray[moduleCount*4+3][methodCount][1][1][0] != null){
                        content += "var " +contentArray[moduleCount*4+3][methodCount][1][1  ][0].name;
                        for(variablesCount = 1; variablesCount < contentArray[moduleCount*4+3][methodCount][1][1].length; variablesCount++){
                            content += ",\\n        "+ contentArray[moduleCount*4+3][methodCount][1][1][variablesCount].name;
                        }
                        for(returnedVariablesCount = 0; returnedVariablesCount < contentArray[moduleCount*4+3][methodCount][1][1].length; returnedVariablesCount++){
                            if(contentArray[moduleCount*4+3][methodCount][1][1][returnedVariablesCount].getsReturned){
                                if(noVarReturnedYet){
                                    content +="\\n return "+ contentArray[moduleCount*4+3][methodCount][1][1][returnedVariablesCount].name;
                                }else{ content += ","+ contentArray[moduleCount*4+3][methodCount][1][1][returnedVariablesCount].name;}
                            }
                        }
                        if(!noVarReturnedYet){
                            content += ";\\n\\n";
                        }
                    }
                }

                content += ";\\n\\n\\n      }\\n\\n\\n        ";


            }


        }

       
        for(returnedClassVariablesCount = 0; returnedClassVariablesCount <contentArray[(4*moduleCount + 2)].length; returnedClassVariablesCount++){
           
            if(contentArray[(4*moduleCount + 2)][returnedClassVariablesCount].getsReturned){
                content += "that." +contentArray[(4*moduleCount + 2)][returnedClassVariablesCount].name+" = "+ contentArray[(4*moduleCount + 2)][returnedClassVariablesCount].name+";\\n";
            }
        }

        content += "\\n\\n       return that;}";
        
        return content;
    }





       


    that.parseJsContent = parseJsContent;
    
 
    return that;
};