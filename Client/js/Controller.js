var App = App||{};
App.Controller = function(){

    var that = {},  
        nameSpaceCallback,
        moduleAddedCallback,
        functionAddedCallback,
        parameterAddedCallback,
        variableAddedCallback,
        nameSpaceButton,
        nameSpaceInput,
        moduleNameButton,
        moduleNameInput,
        functionNameButton,
        functionNameInput,
        parameterNameButton,
        parameterNameInput,
        variableNameButton,
        variableNameInput,
        onFinishCallback,
        finishButton,
        classParameterButton,
        classVarButton,
        classParInput,
        classVarInput,
        classVarCallback,
        classParameterCallback,
        confirmFunctionButton,
        confirmModuleButton,
        moduleConfirmedCallback,
        functionConfirmedCallback,
        classVarReturnInput,
        methodVarReturnInput;
        
        
        
    
    function init(options){
        nameSpaceButton = options.nameSpaceButton;
        moduleNameButton = options.moduleNameButton;
        functionNameButton = options.functionNameButton;
        parameterNameButton = options.parameterNameButton;
        variableNameButton = options.variableNameButton;
        finishButton = options.finishButton;
        classVarButton = options.classVarButton;
        classParameterButton = options.classParameterButton;
        confirmFunctionButton = options.confirmFunctionButton;
        confirmModuleButton = options.confirmModuleButton;
        registerListeners();
        
        return that;
    }
    
    function setNameSpaceCallback(callback){
        nameSpaceCallback = callback;
    }
    
    function setModuleAddedCallback(callback){
        moduleAddedCallback = callback;
    }
    
    function setFunctionAddedCallback(callback){
        functionAddedCallback = callback;
    }
    
    function setParameterAddedCallback(callback){
        parameterAddedCallback = callback;
    }

    function setVariableAddedCallback(callback){
        variableAddedCallback = callback;
    }

    function setModuleConfirmedCallback(callback){
        moduleConfirmedCallback = callback;

    }

    function setFunctionConfirmedCallback(callback){
        functionConfirmedCallback = callback;
    }
     

    function onNameConfirmed(){
        nameSpaceInput = document.getElementById("app_name_input").value;
        document.getElementById("add_nameSpace_button").disabled = true;
        nameSpaceCallback(nameSpaceInput);
        
    }
    
    function onModuleAdded(){
        moduleNameInput = document.getElementById("module_name_input").value;
        document.getElementById("add_module_button").disabled = true;
        moduleAddedCallback(moduleNameInput);
    }
    
    function onFunctionAdded(){
        functionNameInput = document.getElementById("function_name_input").value;
        document.getElementById("add_function_button").disabled = true;
        functionAddedCallback(functionNameInput);
        
    }
    
    function onParameterAdded(){
        parameterNameInput = document.getElementById("parameter_name_input").value;
        parameterAddedCallback(parameterNameInput);
        document.getElementById("parameter_name_input").value = "";

    }

    function onVariableAdded(){
        variableNameInput = document.getElementById("variable_name_input").value;
        methodVarReturnInput = document.getElementById("check_box_method_var").checked;
        variableAddedCallback(variableNameInput, methodVarReturnInput);
        document.getElementById("variable_name_input").value = "";
    }
    
    function setOnFinishCallback(callback){
        onFinishCallback = callback;
    }
    
    function onFinish(){
        onFinishCallback();
    }
    
    function setOnClassParameterCallback(callback){
        classParameterCallback = callback;
    }
    
    function onClassParameterAdded(){
        classParInput = document.getElementById("class_par_input").value;
        classParameterCallback(classParInput);
        document.getElementById("class_par_input").value = "";
    }
    
    function setOnClassVarCallback(callback){
        classVarCallback = callback;
    }
    
    function onClassVarAdded(){
        classVarInput = document.getElementById("class_var_input").value;
        classVarReturnInput = document.getElementById("check_box_klassen_var").checked;
        classVarCallback(classVarInput, classVarReturnInput);
        document.getElementById("class_var_input").value = "";

    }
    
    function onFunctionConfirmed(){
        document.getElementById("add_function_button").disabled = false;
        document.getElementById("function_name_input").value = "";
        functionConfirmedCallback();

    }
    
    function onModuleConfirmed(){
        document.getElementById("add_module_button").disabled = false;
        document.getElementById("module_name_input").value = "";
        moduleConfirmedCallback();
    }
    
 
        
    function registerListeners(){
        nameSpaceButton.addEventListener("click", onNameConfirmed);
        moduleNameButton.addEventListener("click", onModuleAdded);
        functionNameButton.addEventListener("click", onFunctionAdded);
        parameterNameButton.addEventListener("click", onParameterAdded);
        variableNameButton.addEventListener("click", onVariableAdded);
        finishButton.addEventListener("click", onFinish);
        classParameterButton.addEventListener("click",onClassParameterAdded);
        classVarButton.addEventListener("click",onClassVarAdded);
        confirmFunctionButton.addEventListener("click",onFunctionConfirmed);
        confirmModuleButton.addEventListener("click",onModuleConfirmed);
        
    }

    that.init = init;
    that.setNameSpaceCallback = setNameSpaceCallback;
    that.setModuleAddedCallback = setModuleAddedCallback;
    that.setFunctionAddedCallback = setFunctionAddedCallback;
    that.setParameterAddedCallback = setParameterAddedCallback;
    that.setVariableAddedCallback = setVariableAddedCallback;
    that.setOnFinishCallback = setOnFinishCallback;
    that.setOnClassParameterCallback = setOnClassParameterCallback;
    that.setOnClassVarCallback = setOnClassVarCallback;
    that.setModuleConfirmedCallback = setModuleConfirmedCallback;
    that.setFunctionConfirmedCallback = setFunctionConfirmedCallback;
    
    
    return that;
	
};