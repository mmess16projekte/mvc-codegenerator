var App = App||{};
App.MVCGenerator = (function() {
    var that = {},
        listView,
        userInputView,
        controller,
        codeWriter,
        model,
        jsonGenerator,
        currentModule,
        currentFunction,
        currentPar=[],
        currentVar=[],
        methodVariable;


        /* eslint-env browser */
    
    
       

    function addApp(appName){ 
        listView.getUserAppName(appName);
    }
    
    function addModule(moduleName){
        currentModule=moduleName;
        model.addModuleToArray(moduleName);
        userInputView.setFunctionVisibility();
    }
    
    function addFunction(functionName){
        currentFunction = functionName;
        userInputView.setVisibilityForInput();
        
    }
    
    function addParameter(parameterName){
        currentPar.push(parameterName);
        userInputView.getParameterList(parameterName);
    }

    function addVariable(variableName, isReturned){
        methodVariable = {"name" : variableName,
                          "getsReturned" : isReturned};
        currentVar.push(methodVariable);
        userInputView.getVariableList(variableName);
    }
    
    function addClassVariable(classVarName, isReturned){
        model.addClassVariableToArray(classVarName, isReturned);
    }
    
    function addClassParameter(classParName){
        model.addClassParameterToArray(classParName);
    }

    function getModule(){
        userInputView.setInvisibility();
        listView.getValues(model.getArray());
    }

    function clearFunction(){
        var control,
            variableNames,
            i;
        if(currentVar[0]){
            variableNames = currentVar[0].name;

        }
        for(i = 1; i < currentVar.length;i++){
            variableNames += ","+currentVar[i].name;
        }
        control = window.confirm("Im Modul " +currentModule + " Funktion " + currentFunction +" mit Parameter: " + currentPar + " und Variablen: " + variableNames + " erstellen?");
        if(control === true){
            saveFunctionToArray();
            userInputView.clearList();
        } else{
            document.getElementById("function_name_input").value = currentFunction;
            
            userInputView.setListener();
        }
        

    }

    function removeFromList(index, list){
        if (list === "parameter"){
            currentPar.splice(index,1);
        } else if(list === "variable"){
            currentVar.splice(index,1);
        }

    }

    function saveFunctionToArray(){
        var i;
        model.addFunctionToArray(currentFunction);
        for(i=0;i<currentPar.length;i++){
            model.addParameterToArray(currentPar[i]);
        }
        for(i=0;i<currentVar.length;i++){
            model.addVariablesToArray(currentVar[i]);
        }
        currentPar.splice(0);
        currentVar.splice(0);
    }
		
    function sendArray(){

        var modelContentArray = model.getArray(),
            content =[],
            newClass,
            currentClassArray,
            arrayLength = modelContentArray.length,
            text,
            classCount,
            socket,
            encodedUri,
            link;

        for(classCount = 0; classCount*4 < arrayLength-2; classCount ++){
            
            currentClassArray = [modelContentArray[0], modelContentArray[1], modelContentArray[2], modelContentArray[3]]; 
            modelContentArray.splice(0,4);
            newClass ={content: codeWriter.parseJsContent(currentClassArray), name: currentClassArray[0], filetype: "js"};
       
            content.push(newClass);
        
        }
        
        
        text = jsonGenerator.parseStrings(content);
    
        JSON.parse(text);
        socket = io.connect();
        socket.emit("client_data", text);
        
        socket.on("recieveZip",function(content){
            
            encodedUri = content;  
            link = document.createElement("a");  
            link.setAttribute("href", encodedUri);  
            link.setAttribute("download", "app.zip");  
            link.click(); 
            window.location.replace("http://132.199.139.24:8765");
        
        });
                 
    }
    
    

    function init(){
        initUserInput();
        initListView();
        initController();
        initCodeWriter();
        initModel();
        jsonGenerator = App.JsonGenerator();

    }

    function initUserInput(){
        userInputView = App.UserInputView().init({
            parameterList: document.querySelector(".list_modul .list_parameter"),
            variableList:  document.querySelector(".list_modul .list_variables"),
            classParameters: document.querySelector(".klassen_parameter"),
            classVariables: document.querySelector(".klassen_variablen"),
            list_show: document.getElementsByClassName("list_modul"),
            functions: document.querySelector(".function_name"),
            parameters: document.querySelector(".parameters_name"),
            variabels:document.querySelector(".variables_name"),
            functionButton: document.getElementById("confirm_function_button"),
            modulButton: document.getElementById("confirm_module_button"),
            projectButton: document.getElementById("finish_button"),
            deleteText: document.getElementById("delete")
        }); 

        userInputView.setOnListClickedCallback(removeFromList);
    }	
    

    function initListView(){
        listView = App.ListView().init({
            overviewList: document.getElementsByClassName("overview"),
            showAppName: document.getElementById("show_appName"),
            modulsListed: document.querySelector(".overview .application_overview .list_moduls")
        });
    }

    function initController(){
        controller = App.Controller().init({
            nameSpaceButton: document.getElementById("add_nameSpace_button"),
            moduleNameButton: document.getElementById("add_module_button"),
            functionNameButton: document.getElementById("add_function_button"),
            parameterNameButton: document.getElementById("add_parameter_button"),
            variableNameButton: document.getElementById("add_variable_button"),
            finishButton: document.getElementById("finish_button"),
            classParameterButton: document.getElementById("add_class_par_button"),
            classVarButton: document.getElementById("add_class_var_button"),
            confirmFunctionButton: document.getElementById("confirm_function_button"),
            confirmModuleButton: document.getElementById("confirm_module_button")
            
        });
        controller.setNameSpaceCallback(addApp);
        controller.setModuleAddedCallback(addModule);
        controller.setFunctionAddedCallback(addFunction);
        controller.setParameterAddedCallback(addParameter);
        controller.setVariableAddedCallback(addVariable);
        controller.setOnFinishCallback(sendArray);
        controller.setOnClassParameterCallback(addClassParameter);
        controller.setOnClassVarCallback(addClassVariable);
        controller.setModuleConfirmedCallback(getModule);
        controller.setFunctionConfirmedCallback(clearFunction);
       
    }

    function initCodeWriter(){
    
        codeWriter = App.CodeWriter(); //klassenParameter: ["1","2",...], methodenArray[methode1,methode2,...];, methode1:["name",["paramenter1","parameter2",...],["variable1", "variable2",...]]
   
    }



    function initModel(){
        model = App.Model().init();
    }



    that.init = init;
    return that;
}());

