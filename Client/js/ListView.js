var App = App||{};
App.ListView = function(){

    var that = {},
        overview,
        appName,
        moduleList,
        showAppName;

    function init(options){

        overview = options.overviewList;
        showAppName = options.showAppName; 
        moduleList = options.modulsListed;
        return that;
    }


    function getValues(moduleArray){

        var i,j,k,m, modulString = "", functionString= "", current, functions = [];

        clearList();

        for(i=0;i<moduleArray.length;i+=4){
            current = moduleArray[i];
            modulString = current;

            if(moduleArray[1+i].length > 0 || moduleArray[2+i].length>0){
                modulString += " (";
                for (j=0;j<moduleArray[1+i].length;j++){
                    current = moduleArray[1+i][j];
                    modulString += current;
                    if (j < moduleArray[1+i].length -1){
                        modulString += ", ";
                    }
                }
                if(moduleArray[1+i].length > 0 && moduleArray[2+i].length>0){
                    modulString += "/ ";
                }    

                for (k=0;k<moduleArray[2+i].length;k++){
                    current = moduleArray[2+i][k].name;
                    modulString += current;
                    if(k<moduleArray[2+i].length-1){
                        modulString += ", ";
                    }
                }
                
                modulString += ")";
            }

            for (m=0;m<moduleArray[3+i].length;m++){
                current = moduleArray[3+i][m][0].name;
                functionString = current;

                if(moduleArray[3+i][m][1][0].length>0 || moduleArray[3+i][m][1][1].length>0){
                    functionString +=" (";
                    for (j=0;j<moduleArray[3+i][m][1][0].length;j++){
                        current = moduleArray[3+i][m][1][0][j];
                        functionString += current;
                        if(j<moduleArray[3+i][m][1][0].length-1){
                            functionString += ", ";
                        }
                    }
                    if(moduleArray[3+i][m][1][0].length>0 && moduleArray[3+i][m][1][1].length>0){
                        functionString += "/ ";
                    }

                    for (k=0;k<moduleArray[3+i][m][1][1].length;k++){
                        current = moduleArray[3+i][m][1][1][k].name;
                        functionString+= current;
                        if(k<moduleArray[3+i][m][1][1].length-1){
                            functionString += ", ";
                        }
                    }	
                    
                    functionString += ")";
                }	
                functions.push(functionString);

            }
            getList(modulString, functions);
            functions.splice(0);
        }
    }
    
    


    function getUserAppName(userAppName){
        var i;
        for(i=0;i<overview.length;i++){
            overview[i].style.visibility = "visible";
        }
        appName = userAppName;
        showAppName.innerHTML = showAppName.innerHTML + " " + appName;
    }

	
    function getList(modulName, functions){
        var newModul,i, newFunction;


        newModul = document.createElement("li");
        newModul.innerHTML = modulName;
        moduleList.appendChild(newModul);

        for(i=0;i<functions.length;i++){
            newFunction = document.createElement("li");
            newFunction.innerHTML = functions[i];
            newFunction.style.margin ="15px";
            moduleList.appendChild(newFunction);
        }       
    }

    function clearList(){
        while (moduleList.firstChild){
            moduleList.removeChild(moduleList.firstChild);
        }
    }	


    

    that.init = init;
    that.getUserAppName = getUserAppName;
    that.getValues = getValues;
    return that;
	
};