var App = App||{};
App.JsonGenerator = function(){

    var that = {},
        returnJson = "";


    function parseStrings(data){

        var classCounter;
        
        returnJson += '{ "classes" : [';
        
        if(data!=null){
            
            returnJson+=  '{"className" : "'+data[0].name+'",'
                +'"fileType" : ".'+data[0].filetype+'",'
                +'"classContent" : "'+data[0].content+'"}';
         


            for(classCounter = 1; classCounter< data.length; classCounter++){
                       
                returnJson+=         
                        ',{"className" : "'+data[classCounter].name+'",'
                        +'"fileType" : ".'+data[classCounter].filetype+'",'
                        +'"classContent" : "'+data[classCounter].content+'"}';
        
       
            }
        }
        returnJson += "]}";
        return returnJson;
    
    }

    that.parseStrings =parseStrings;

    return that;
};
