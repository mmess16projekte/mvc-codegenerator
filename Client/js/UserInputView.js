var App = App||{};
App.UserInputView = function(){

    var that = [],
        parametersList,
        listParametersObjects = [],
        variablesList,
        listVariablesObjects = [],
        classParameter,
        classVariables,
        functions,
        parameters,
        variables,
        finishButton,
        modulButton,
        functionButton,
        showList,
        deleteText,
        onListClickedCallback;
    
    function init(options){
        parametersList = options.parameterList;
        variablesList = options.variableList;
        classParameter = options.classParameters;
        classVariables = options.classVariables;
        functions = options.functions;
        parameters = options.parameters;
        variables = options.variabels;
        modulButton = options.modulButton;
        functionButton = options.functionButton;
        modulButton = options.modulButton;
        finishButton = options.projectButton,
        showList = options.list_show;
        deleteText = options.deleteText;

        return that;     
    }

    function setOnListClickedCallback(callback){
        onListClickedCallback = callback;
    }


    function setInvisibility(){
        var i;
        classParameter.style.visibility = "hidden";
        classVariables.style.visibility = "hidden";
        functions.style.visibility = "hidden";
        modulButton.style.visibility = "hidden";
        parameters.style.visibility = "hidden";
        variables.style.visibility = "hidden";
        functionButton.style.visibility = "hidden";
        for(i=0;i<showList.length;i++){
            showList[i].style.visibility = "hidden";
        }
    }


    function setFunctionVisibility(){
        classParameter.style.visibility = "visible";
        classVariables.style.visibility = "visible";
        functions.style.visibility = "visible";
        
        finishButton.style.visibility = "visible";
        


    }

    function setVisibilityForInput(){
        var i;
        parameters.style.visibility = "visible";
        variables.style.visibility = "visible";
        modulButton.style.visibility = "visible";
        functionButton.style.visibility = "visible";

        for(i=0;i<showList.length;i++){
            showList[i].style.visibility = "visible";
        }
    }

    function getParameterList(parameterName){
        var getNewParameterObject;
        getNewParameterObject = document.createElement("div");
        getNewParameterObject.innerHTML = parameterName;
        listParametersObjects.push(getNewParameterObject);
        parametersList.appendChild(getNewParameterObject);

    }

    function getVariableList(variableName){
        var getNewVariableObject;
        getNewVariableObject = document.createElement("div");
        getNewVariableObject.innerHTML = variableName;
        listVariablesObjects.push(getNewVariableObject);
        variablesList.appendChild(getNewVariableObject);

    }

    function clearList(){
        deleteText.style.visibility ="hidden";
        while(parametersList.firstChild){
            parametersList.removeChild(parametersList.firstChild);
        }

        while(variablesList.firstChild){
            variablesList.removeChild(variablesList.firstChild);
        }

    }

    function onClick(event){
        var i;
        for(i=0;i<parametersList.children.length;i++){
            if(parametersList.children[i] === event.target){
                parametersList.removeChild(parametersList.children[i]);
                onListClickedCallback(i,"parameter");
            }
        }
        for(i=0;i<variablesList.children.length;i++){
            if(variablesList.children[i] === event.target){
                variablesList.removeChild(variablesList.children[i]);
                onListClickedCallback(i,"variable");
            }
        }



    }

    function setListener(){
        var i;
        deleteText.style.visibility ="visible";
        for (i=0;i<parametersList.children.length;i++){
            parametersList.children[i].addEventListener("click", onClick);
        }
        for(i=0;i<variablesList.children.length;i++){
            variablesList.children[i].addEventListener("click", onClick);
        }
    }

    
    that.init = init;
    that.getParameterList = getParameterList;
    that.getVariableList = getVariableList;
    that.setFunctionVisibility = setFunctionVisibility;
    that.setVisibilityForInput = setVisibilityForInput;
    that.setInvisibility = setInvisibility;
    that.clearList = clearList;
    that.setListener = setListener;
    that.setOnListClickedCallback = setOnListClickedCallback;
    return that;
};