var FileCreator = function() {

    var that = {},
        admZip = require("adm-zip"),
        zip = new admZip(),
        fileNames = [];

    function init(){
        
        zip.addFile("app/", new Buffer(""), "comment");
        zip.addFile("app/js/", new Buffer(""), "comment");
        zip.addFile("app/res/", new Buffer(""), "comment");
        zip.addFile("app/index.html", new Buffer(getIndexContent()), "comment");
        zip.addFile("app/res/app.css", new Buffer(getCSSContent()), "comment");


    }

    function getCSSContent(){

        cssContent = "html {\nheight: 100%;\n margin: 0;\n}\n " + "body{\n}\n " + "header {\n}\n";
        return cssContent;

    }


    function createZIPFile(){
     
        var prettyBuffer = zip.toBuffer()
        var uriContent = "data:application/zip;base64," + prettyBuffer.toString('base64');
        
        return uriContent;
                            
    }
	
    function writeFile(fileName, fileContent, fileType){
        fileNames.push(fileName);
        zip.addFile("app/js/"+fileName+fileType, new Buffer(fileContent), "comment");
              
    }

 
    function getIndexContent(){
        content = "<!DOCTYPE html>\n<html>\n<head>\n" + "<meta charset='utf-8'>\<title>" + "appName" +"</title>\n<link rel='stylesheet' href='/res/app.css'>\n" + 
		"</head>\n<body>\n<header>\n<h1 class='title'>" +"app"+ "</h1>\n</header>\n<content>\n </content>\n"
        for(var i=0;i<fileNames.length;i++){
            content += "<script src='js/'" + fileNames[i]+".js'></script>\n";
        }
        content += "</body>\n</html>\n";

        return content;

    }



    that.init = init;	
    that.writeFile = writeFile;
    that.createZIPFile = createZIPFile;
    return that;	
};

module.exports = FileCreator;